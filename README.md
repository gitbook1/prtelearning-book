# 核心反應訓練計畫行動裝置App與網站管理說明手冊

本文件主要針對「PRTElearning」應用程式使用、程式開發實設計的對應結構與「mcntu」核心反應計畫網站後台操作管理等使用上可能會遇到的問題做紀錄。
[線上檢視版](https://san_huang.gitlab.io/prtelearning-book/)

###本案相關紀錄檔案
由於影片檔過大，暫時上傳到google drive然後後續整理紀錄過程中再逐一轉到youtube或是直接從google drive載出播放。
* [影片檔列表](resource/videos.md)
* [輸出檔案列表](resource/files.md)
* [尚未整理圖檔列表](resource/images.md)


## PRTElearning程式介紹

使用React-Native v0.58搭配Expo SDK v32.0版進行開發，使用的第三方套件可參考/package.json內**dependencies片段**所列。

## 程式剩餘需求、檢測修正項目

#### Chrome不明原因會直接替換網頁上顯示的文字
註意 => 注意
糢仿 => 模仿
起牀 => 起床



#### 網站後端管理系統
 ☐ 後台家長操作紀錄與檢視各章節上傳影片


### 發布獨立安裝程式
 ### Android平板 / 手機
  (.apk安裝檔)
  測試裝置以Samsung Galaxy Tab J/Android5.1為主
  以Infucs手機為主，確認所有功能可操作與畫面可檢視
 ### iOS平板 / 手機
  (.ipa安裝檔)，透過TestFlight程式安裝測試版本，有效期限90天，逾期可能得重新發佈版本


