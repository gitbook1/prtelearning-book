# React-Navigation v2應用技巧

在PRTElearning內業主希望盡可能在多處提供跳轉章節內容的連結功能，共建構了使用SubMenu元件、Drawer navigation

## 修正側選單連結不正常問題

原因：參考官方對於[Navigation Lifecycle下Example scenario](https://reactnavigation.org/docs/en/2.x/navigation-lifecycle.html#example-scenario)說明內容，得知
**第一問題**
在於我之前呼叫fetch取得網站內容放置在`componentWillUnmount()`並不能確保每次頁面載入都會執行！
**第二問題**
We start on the HomeScreen and navigate to DetailsScreen. Then we use the tab bar to switch to the SettingsScreen and navigate to ProfileScreen. After this sequence of operations is done, all 4 of the screens are mounted! If you use the tab bar to switch back to the HomeStack, you'll notice you'll be presented with the DetailsScreen - the navigation state of the HomeStack has been preserved!

解法：根據第二問題，推測使用 navigation.navigate() 來連結已掛載(mounted)Stack並無法保證會顯示預期內容，目前修正作法是直接每次重建navigation.routes確保即便是已經產生過的statck仍會再次以正確參數呼叫建立。因此在原本使用 `this.props.navigation.navigate()`部分，改用以下作法呼叫
```javascript
// 其中配置兩個NavigationActions.navigate()是為了回上頁可以正確連結！
this.props.navigation.dispatch(StackActions.reset({
  index: 1,
  actions: [
  NavigationActions.navigate({routeName : 'screen_0',}),
  NavigationActions.navigate({
    routeName : 'screen_83',
    params : {
      categoryId : 83,
      articleId : 27,
      pagetitle : '簡介'
    }
  })],
}));
// 但上述做法可能會有無法連續兩次回上頁狀況！
```


## 取得當下整個routes結構語法

必須透過stackAction API

```javascript
// 整個routes結構（不包含目前所在state）
this.props.navigation.dangerouslyGetParent().state.routes
```

待解問題：
實際上應該是可以用navigation.push()而非重建方式，但目前很可能是因為我主要建立navigation架構不正確，透過DrawerNaviagtion部分包覆StatckNavigation導致沒辦法使用navigation.push()（在側選單連結時this.props應該是DrawerNaviagtion而非StatckNavigation，**但navigation.push()是StatckNavigation的api**）
