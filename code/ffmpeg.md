# CentOS 7.3更新ffmpeg版本

2018年架設伺服器環境時安裝的ffmpeg版本為_2.6.8_

```bash
# ffmpeg --version
ffmpeg version 2.6.8 Copyright (c) 2000-2016 the FFmpeg developers
  built with gcc 4.8.5 (GCC) 20150623 (Red Hat 4.8.5-4)
  configuration: --prefix=/usr --bindir=/usr/bin --datadir=/usr/share/ffmpeg --incdir=/usr/include/ffmpeg --libdir=/usr/lib64 --mandir=/usr/share/man --arch=x86_64 --optflags='-O2 -g -pipe -Wall -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector-strong --param=ssp-buffer-size=4 -grecord-gcc-switches -m64 -mtune=generic' --enable-bzlib --disable-crystalhd --enable-gnutls --enable-ladspa --enable-libass --enable-libcdio --enable-libdc1394 --enable-libfaac --enable-nonfree --enable-libfdk-aac --enable-nonfree --disable-indev=jack --enable-libfreetype --enable-libgsm --enable-libmp3lame --enable-openal --enable-libopenjpeg --enable-libopus --enable-libpulse --enable-libschroedinger --enable-libsoxr --enable-libspeex --enable-libtheora --enable-libvorbis --enable-libv4l2 --enable-libx264 --enable-libx265 --enable-libxvid --enable-x11grab --enable-avfilter --enable-avresample --enable-postproc --enable-pthreads --disable-static --enable-shared --enable-gpl --disable-debug --disable-stripping --shlibdir=/usr/lib64 --enable-runtime-cpudetect
  libavutil      54. 20.100 / 54. 20.100
  libavcodec     56. 26.100 / 56. 26.100
  libavformat    56. 25.101 / 56. 25.101
  libavdevice    56.  4.100 / 56.  4.100
  libavfilter     5. 11.102 /  5. 11.102
  libavresample   2.  1.  0 /  2.  1.  0
  libswscale      3.  1.101 /  3.  1.101
  libswresample   1.  1.100 /  1.  1.100
  libpostproc    53.  3.100 / 53.  3.100
Unrecognized option '-version'.
Error splitting the argument list: Option not found
```

---

###當時安裝方式使用以下步驟指令

新增`nux-dextop`repo以便安裝CentOS7下ffmpeg rpm檔

```bash
#以下必須使用root身份進行安裝
$rpm -Uvh http://li.nux.ro/download/nux/dextop/el7/x86_64/nux-dextop-release-0-5.el7.nux.noarch.rpm
正在擷取 http://li.nux.ro/download/nux/dextop/el7/x86_64/nux-dextop-release-0-5.el7.nux.noarch.rpm
警告：/var/tmp/rpm-tmp.OZZDwY: 表頭 V4 RSA/SHA1 Signature, key ID 85c6cd8a: NOKEY
錯誤：相依關係失敗：
	epel-release 被 nux-dextop-release-0-5.el7.nux.noarch 需要

#故必須先安裝epel-release
$yum install epel-release -y

$rpm -Uvh http://li.nux.ro/download/nux/dextop/el7/x86_64/nux-dextop-release-0-5.el7.nux.noarch.rpm
正在擷取 http://li.nux.ro/download/nux/dextop/el7/x86_64/nux-dextop-release-0-5.el7.nux.noarch.rpm
警告：/var/tmp/rpm-tmp.SdfuK7: 表頭 V4 RSA/SHA1 Signature, key ID 85c6cd8a: NOKEY
正在準備…                       ################################# [100%]
Updating / installing...
   1:nux-dextop-release-0-5.el7.nux   ################################# [100%]

```

###安裝ffmpeg與相依套件

```bash
$ yum install ffmpeg ffmpeg-devel -y
==============================================================================================
 Package                   Arch     Version                                Repository    Size
==============================================================================================
Installing:
 ffmpeg                    x86_64   2.8.15-1.el7.nux                       nux-dextop   1.3 M
 ffmpeg-devel              x86_64   2.8.15-1.el7.nux                       nux-dextop   678 k
Installing for dependencies:
 SDL                       x86_64   1.2.15-14.el7                          base         204 k
 dejavu-fonts-common       noarch   2.33-6.el7                             base          64 k
 dejavu-sans-fonts         noarch   2.33-6.el7                             base         1.4 M
 ffmpeg-libs               x86_64   2.8.15-1.el7.nux                       nux-dextop   5.5 M
 flac-libs                 x86_64   1.3.0-5.el7_1                          base         169 k
 fontconfig                x86_64   2.13.0-4.3.el7                         base         254 k
 fontpackages-filesystem   noarch   1.44-8.el7                             base         9.9 k
 fribidi                   x86_64   1.0.2-1.el7                            base          79 k
 graphite2                 x86_64   1.3.10-1.el7_3                         base         115 k
 gsm                       x86_64   1.0.13-11.el7                          base          30 k
 harfbuzz                  x86_64   1.7.5-2.el7                            base         267 k
 lame-libs                 x86_64   3.100-1.el7                            epel         354 k
 libXdamage                x86_64   1.1.4-4.1.el7                          base          20 k
 libXfixes                 x86_64   5.0.3-1.el7                            base          18 k
 libXi                     x86_64   1.7.9-1.el7                            base          40 k
 libXtst                   x86_64   1.2.3-1.el7                            base          20 k
 libXv                     x86_64   1.0.11-1.el7                           base          18 k
 libXxf86vm                x86_64   1.1.4-1.el7                            base          18 k
 libass                    x86_64   0.13.4-6.el7                           epel          99 k
 libasyncns                x86_64   0.8-7.el7                              base          26 k
 libavdevice               x86_64   2.8.15-1.el7.nux                       nux-dextop    73 k
 libcdio                   x86_64   0.92-3.el7                             base         236 k
 libcdio-paranoia          x86_64   10.2+0.90-11.el7                       base          70 k
 libdc1394                 x86_64   2.2.2-3.el7                            epel         121 k
 libglvnd                  x86_64   1:1.0.1-0.8.git5baa1e5.el7             base          89 k
 libglvnd-egl              x86_64   1:1.0.1-0.8.git5baa1e5.el7             base          44 k
 libglvnd-glx              x86_64   1:1.0.1-0.8.git5baa1e5.el7             base         125 k
 libjpeg-turbo             x86_64   1.2.90-6.el7                           base         134 k
 libogg                    x86_64   2:1.3.0-7.el7                          base          24 k
 libraw1394                x86_64   2.1.0-2.el7                            base          63 k
 libsndfile                x86_64   1.0.25-10.el7                          base         149 k
 libtheora                 x86_64   1:1.1.1-8.el7                          base         136 k
 libusbx                   x86_64   1.0.21-1.el7                           base          61 k
 libv4l                    x86_64   0.9.5-4.el7                            base         194 k
 libva                     x86_64   1.8.3-1.el7                            base          80 k
 libvdpau                  x86_64   1.1.1-3.el7                            base          34 k
 libvorbis                 x86_64   1:1.3.3-8.el7.1                        base         204 k
 libwayland-client         x86_64   1.15.0-1.el7                           base          33 k
 libwayland-server         x86_64   1.15.0-1.el7                           base          39 k
 libxshmfence              x86_64   1.2-1.el7                              base         7.2 k
 mesa-libEGL               x86_64   18.0.5-4.el7_6                         updates      102 k
 mesa-libGL                x86_64   18.0.5-4.el7_6                         updates      162 k
 mesa-libgbm               x86_64   18.0.5-4.el7_6                         updates       38 k
 mesa-libglapi             x86_64   18.0.5-4.el7_6                         updates       44 k
 openal-soft               x86_64   1.16.0-3.el7                           epel         282 k
 opencore-amr              x86_64   0.1.3-3.el7.nux                        nux-dextop   172 k
 openjpeg-libs             x86_64   1.5.1-18.el7                           base          86 k
 opus                      x86_64   1.0.2-6.el7                            base         630 k
 orc                       x86_64   0.4.26-1.el7                           base         166 k
 pulseaudio-libs           x86_64   10.0-5.el7                             base         651 k
 schroedinger              x86_64   1.0.11-4.el7                           epel         291 k
 soxr                      x86_64   0.1.2-1.el7                            epel          77 k
 speex                     x86_64   1.2-0.19.rc1.el7                       base          98 k
 vo-amrwbenc               x86_64   0.1.2-1.el7.nux                        nux-dextop    70 k
 x264-libs                 x86_64   0.142-11.20141221git6a301b6.el7.nux    nux-dextop   570 k
 x265-libs                 x86_64   1.9-1.el7.nux                          nux-dextop   1.5 M
 xvidcore                  x86_64   1.3.2-5.el7.nux                        nux-dextop   258 k
Updating for dependencies:
 dbus                      x86_64   1:1.10.24-12.el7                       base         245 k
 dbus-libs                 x86_64   1:1.10.24-12.el7                       base         169 k
 freetype                  x86_64   2.8-12.el7_6.1                         updates      380 k
 freetype-devel            x86_64   2.8-12.el7_6.1                         updates      447 k
 libdrm                    x86_64   2.4.91-3.el7                           base         153 k

Transaction Summary
==============================================================================================
Install  2 Packages (+57 Dependent packages)
Upgrade             (  5 Dependent packages)

Total size: 19 M
Total download size: 18 M

Installed:
  ffmpeg.x86_64 0:2.8.15-1.el7.nux           ffmpeg-devel.x86_64 0:2.8.15-1.el7.nux

Dependency Installed:
  SDL.x86_64 0:1.2.15-14.el7
  dejavu-fonts-common.noarch 0:2.33-6.el7
  dejavu-sans-fonts.noarch 0:2.33-6.el7
  ffmpeg-libs.x86_64 0:2.8.15-1.el7.nux
  flac-libs.x86_64 0:1.3.0-5.el7_1
  fontconfig.x86_64 0:2.13.0-4.3.el7
  fontpackages-filesystem.noarch 0:1.44-8.el7
  fribidi.x86_64 0:1.0.2-1.el7
  graphite2.x86_64 0:1.3.10-1.el7_3
  gsm.x86_64 0:1.0.13-11.el7
  harfbuzz.x86_64 0:1.7.5-2.el7
  lame-libs.x86_64 0:3.100-1.el7
  libXdamage.x86_64 0:1.1.4-4.1.el7
  libXfixes.x86_64 0:5.0.3-1.el7
  libXi.x86_64 0:1.7.9-1.el7
  libXtst.x86_64 0:1.2.3-1.el7
  libXv.x86_64 0:1.0.11-1.el7
  libXxf86vm.x86_64 0:1.1.4-1.el7
  libass.x86_64 0:0.13.4-6.el7
  libasyncns.x86_64 0:0.8-7.el7
  libavdevice.x86_64 0:2.8.15-1.el7.nux
  libcdio.x86_64 0:0.92-3.el7
  libcdio-paranoia.x86_64 0:10.2+0.90-11.el7
  libdc1394.x86_64 0:2.2.2-3.el7
  libglvnd.x86_64 1:1.0.1-0.8.git5baa1e5.el7
  libglvnd-egl.x86_64 1:1.0.1-0.8.git5baa1e5.el7
  libglvnd-glx.x86_64 1:1.0.1-0.8.git5baa1e5.el7
  libjpeg-turbo.x86_64 0:1.2.90-6.el7
  libogg.x86_64 2:1.3.0-7.el7
  libraw1394.x86_64 0:2.1.0-2.el7
  libsndfile.x86_64 0:1.0.25-10.el7
  libtheora.x86_64 1:1.1.1-8.el7
  libusbx.x86_64 0:1.0.21-1.el7
  libv4l.x86_64 0:0.9.5-4.el7
  libva.x86_64 0:1.8.3-1.el7
  libvdpau.x86_64 0:1.1.1-3.el7
  libvorbis.x86_64 1:1.3.3-8.el7.1
  libwayland-client.x86_64 0:1.15.0-1.el7
  libwayland-server.x86_64 0:1.15.0-1.el7
  libxshmfence.x86_64 0:1.2-1.el7
  mesa-libEGL.x86_64 0:18.0.5-4.el7_6
  mesa-libGL.x86_64 0:18.0.5-4.el7_6
  mesa-libgbm.x86_64 0:18.0.5-4.el7_6
  mesa-libglapi.x86_64 0:18.0.5-4.el7_6
  openal-soft.x86_64 0:1.16.0-3.el7
  opencore-amr.x86_64 0:0.1.3-3.el7.nux
  openjpeg-libs.x86_64 0:1.5.1-18.el7
  opus.x86_64 0:1.0.2-6.el7
  orc.x86_64 0:0.4.26-1.el7
  pulseaudio-libs.x86_64 0:10.0-5.el7
  schroedinger.x86_64 0:1.0.11-4.el7
  soxr.x86_64 0:0.1.2-1.el7
  speex.x86_64 0:1.2-0.19.rc1.el7
  vo-amrwbenc.x86_64 0:0.1.2-1.el7.nux
  x264-libs.x86_64 0:0.142-11.20141221git6a301b6.el7.nux
  x265-libs.x86_64 0:1.9-1.el7.nux
  xvidcore.x86_64 0:1.3.2-5.el7.nux

Dependency Updated:
  dbus.x86_64 1:1.10.24-12.el7               dbus-libs.x86_64 1:1.10.24-12.el7
  freetype.x86_64 0:2.8-12.el7_6.1           freetype-devel.x86_64 0:2.8-12.el7_6.1
  libdrm.x86_64 0:2.4.91-3.el7

Complete!
```

###確認可使用ffmpeg

```bash
$ ffmpeg --version
ffmpeg version 2.8.15 Copyright (c) 2000-2018 the FFmpeg developers
  built with gcc 4.8.5 (GCC) 20150623 (Red Hat 4.8.5-28)
  configuration: --prefix=/usr --bindir=/usr/bin --datadir=/usr/share/ffmpeg --incdir=/usr/include/ffmpeg --libdir=/usr/lib64 --mandir=/usr/share/man --arch=x86_64 --optflags='-O2 -g -pipe -Wall -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector-strong --param=ssp-buffer-size=4 -grecord-gcc-switches -m64 -mtune=generic' --extra-ldflags='-Wl,-z,relro ' --enable-libopencore-amrnb --enable-libopencore-amrwb --enable-libvo-amrwbenc --enable-version3 --enable-bzlib --disable-crystalhd --enable-gnutls --enable-ladspa --enable-libass --enable-libcdio --enable-libdc1394 --disable-indev=jack --enable-libfreetype --enable-libgsm --enable-libmp3lame --enable-openal --enable-libopenjpeg --enable-libopus --enable-libpulse --enable-libschroedinger --enable-libsoxr --enable-libspeex --enable-libtheora --enable-libvorbis --enable-libv4l2 --enable-libx264 --enable-libx265 --enable-libxvid --enable-x11grab --enable-avfilter --enable-avresample --enable-postproc --enable-pthreads --disable-static --enable-shared --enable-gpl --disable-debug --disable-stripping --shlibdir=/usr/lib64 --enable-runtime-cpudetect
  libavutil      54. 31.100 / 54. 31.100
  libavcodec     56. 60.100 / 56. 60.100
  libavformat    56. 40.101 / 56. 40.101
  libavdevice    56.  4.100 / 56.  4.100
  libavfilter     5. 40.101 /  5. 40.101
  libavresample   2.  1.  0 /  2.  1.  0
  libswscale      3.  1.101 /  3.  1.101
  libswresample   1.  2.101 /  1.  2.101
  libpostproc    53.  3.100 / 53.  3.100
Unrecognized option '-version'.
Error splitting the argument list: Option not found
```