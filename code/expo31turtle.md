# Support init with a specific sdkVersion
  目前最新版本Expo SDK為v33，搭配React-Native v0.58，但在Android 5.1的Pad上似乎有些輸出問題，打算測試降低版本轉移程式碼看看能否解決，或者逐一改寫原本程式確認問題點。
---

### [以SDK v31使用expo init產生專案結構](https://github.com/expo/expo-cli/issues/142)

### Build your own standalone apps with Turtle CLI