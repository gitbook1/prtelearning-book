#ffmpeg參數說明

以下為舊版mcntu使用參數語法
```bash
  $/usr/bin/ffmpeg -y -i /usr/local/lsws/mcntu/html/public/uploads/uservideoes/06_1_1.mpg -async 1 -metadata:s:v:0 start_time=0 -r 24 -b_strategy 1 -bf 3 -g 24 -threads 12 -vcodec libx264 -acodec libfaac -b:v 1000k -refs 6 -coder 1 -sc_threshold 40 -flags +loop -me_range 16 -subq 7 -i_qfactor 0.71 -qcomp 0.6 -qdiff 4 -trellis 1 -b:a 128k -ac 2 -pass 1 -passlogfile /tmp/ffmpeg-passes55bc5d4d71640c6hm5/pass-55bc5d4d71e88 /usr/local/lsws/mcntu/html/public/uploads/uservideoes/output/55bc5d4d70e97-x264.mp4
```
並透過litespeed設定使用者`nobody`使用`crontab`每日執行
```bash
$su nobody
...password

$crontab -e

#...in file
# 每天凌晨1,3,5點執行
* 1,3,5 * * * php /home/mcntu/html/scripts/cli.php main cron
```

##ffmpeg主要參數

####參數設定規則說明
1. 沒有特別宣告(配置)的話，所有的數字參數都以一個字符串代表數字的形式作爲輸入參數接收，所有的數字參數都會帶著一個SI前綴單位。
> 例如`K`, `M`或者`G`
2. 如果 `i`被追加到*SI*的首字母，完整的字母會被當做i的二進制倍數解釋，倍數需要乘以1024，而不是乘以1000。
3. 追加 `B`到SI單位的前面倍數是8。
> 這幾個參數用法都是作爲數字的後綴，例如`KB`,`MiB`,`G`和`B`。
4. 沒有設置值爲ture的boolean類型的參數：
> boolean類型的false可以在選項前面加上`no`來表示。例如使用 `-nofoo`會設置`foo`參數爲boolean類型的false。
5. (影片或聲音)頻`stream`配置通常是**被追加到選項名字後面**並且用**分號**分開的一個字符串。

6. 一個頻參數可以匹配多組頻，因此參數選項是被用到所有頻上的。
```bash
#a:1 這個參數代表對應第二個音頻流，:a代表使用ac3編碼，例如：
-codec:a:1
#因此，音頻將會選擇ac3編碼作爲第二個音頻。
-b:a 128k
對應到全部音頻
```

7. 空的參數配置代表適用於所有頻。
```bash
#例如：
-codec copy
#或者
-codec:copy
#會直接複製所有頻，不需要再次編碼。
```

####通用參數
參數值 | 說明
----|---
-L | show license
-h topic | show help
-? topic | show help
-help topic | show help
--help topic | show help
-version | show version
-buildconf | show build configuration
-formats | show available formats
-devices | show available devices
-codecs | show available codecs
-decoders | show available decoders
-encoders | show available encoders
-bsfs | show available bit stream filters
-protocols | show available protocols
-filters | show available filters
-pix_fmts | show available pixel formats
-layouts | show standard channel layouts
-sample_fmts | show available audio sample formats
-colors | show available color names
-sources device | list sources of the input device
-sinks device | list sinks of the output device
-hwaccels | show available HW acceleration methods


####可能的串流參數如下

```bash
stream_index
```

根據索引匹配
```bash
stream_type[:stream_index]
#例如：
-threads:1 4
#將會爲第二個流設置4個線程處理。
```
其中`stream_type`可以下列參數表示
* 視頻可以選擇`v`或`V`其中一個參數表示
* 音頻參數是`a`表示
* 字幕參數是`s`表示
* 數據的參數爲`d`表示
* 附件的參數爲`t`表示

```bash
p:program_id[:stream_index]
```
如果設置了stream_index，program_id的程序會對應stream_index的串流影片
```bash
m:key[:value]
```
使用元數據中帶有key=value的ag去匹配流。如果沒有value，會匹配此tag(key值)的任何流。

> 根據元數據匹配僅僅會在輸入的文件有用

```bash
-hide_banner
# (global)
```
隱藏每次都會顯示的大量ffmpeg資訊

```bash
-y (global)
```
不需要詢問覆蓋輸出文件

```bash
-n (global)
```
不會覆蓋輸出文件，如果輸出文件已經存在程序立即退出。

```bash
-stream_loop number（input）
```
設置輸入流被循環的次數。循環0次意味著沒有循環，循環-1代表無限循環。

```bash
-c[:stream_specifier] codec (intput/output,per-stream)
#或者
-coedc[:stream_specifier] codec (intput/output,per-stream)
```
爲一個或多個串流選擇一個編碼器(當用在輸出文件的前面時)或一個解碼器(當使用在輸入文件的前面時)。
> codec是一個解碼器或編碼器的名字或特殊值 copy(僅對輸出有效)說明流不會被重新編碼。

#####以下為配置案例

```bash
$ffmpeg -i INPUT -map 0 -c:v libx264 -c:a copy OUTPUT
```
使用libx264編碼所有的影片串流，並複製所有的音頻。

上述內容來自[FFmpeg 參數詳解(二) - 神馬文庫](https://www.smwenku.com/a/5b8d19362b717718833b2d61)並進行繁中處理
> 並未完成所有內容轉換僅針對主要部分紀錄