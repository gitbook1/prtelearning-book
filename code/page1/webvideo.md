# 網站部分處理影片轉檔作法

新版採用以**上傳影片後直接觸發指令**轉檔，並於**背景執行完成後觸發**將結果修改資料表欄位或者儲存錯誤問題於log檔作法。

#### 相關檔案路徑
* /mcntu/apps/tasks/MainTask.php
* /mcntu/apps/backend/controllers/ApiController.php
* /mcntu/public/uploads/avideo

##### 安裝Beanstalk服務

直接透過epel repo即可在CentOS7.3下使用yum安裝

```bash
$yum install --enablerepo=epel beanstalkd.x86_64
Dependencies Resolved

==============================================================================================
 Package                 Arch                Version                  Repository         Size
==============================================================================================
Installing:
 beanstalkd              x86_64              1.10-2.el7               epel               48 k

Transaction Summary
==============================================================================================
Install  1 Package
Total download size: 48 k
Installed size: 94 k

$systemctl status beanstalkd
● beanstalkd.service - Beanstalkd Fast Workqueue Service
   Loaded: loaded (/usr/lib/systemd/system/beanstalkd.service; disabled; vendor preset: disabled)

$systemctl enable beanstalkd
Created symlink from /etc/systemd/system/multi-user.target.wants/beanstalkd.service to /usr/lib/systemd/system/beanstalkd.service.

$systemctl start beanstalkd

$ps aux | grep beanstalkd
beansta+  7027  0.0  0.0   6620   776 ?        Ss   14:27   0:00 /usr/bin/beanstalkd -l 0.0.0.0 -p 11300 -u beanstalkd
root      7029  0.0  0.0 112724   968 pts/0    S+   14:27   0:00 grep --color=auto beanstalkd

$beanstalkd -v
beanstalkd 1.10

```
> 參考[Install Beanstalk on CentOs 7 RHEL 7 Fedora VPS Server](https://www.techtransit.org/install-beanstalk-centos-7-rhel-7-fedora-vps-server/)

##### 在Phalcon Framework內使用cli來執行Queue

1. 定義一個呼叫背景程式的Action
2. 加入ffmpeg指令
3. 取得呼叫背景的jobid並透過`while`語法來偵測該程式是否執行完成
4. 完成片段內配置對應要執行的邏輯
   1. 修改資料表欄位
   2. 儲存錯誤問題於log檔


> 參考[Phalcon Documentation](https://docs.phalconphp.com/3.4/ja-jp/queue)

> 參考[Queue (Beanstalk) - Discussion - Phalcon Framework](https://forum.phalconphp.com/discussion/17897/queue-beanstalk)


---

補充資訊

- [Beanstalk Queue performance - Phalcon Framework](https://forum.phalconphp.com/discussion/6116/beanstalk-queue-performance)