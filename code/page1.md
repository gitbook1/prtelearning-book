# 程式檔案結構說明

2019執行版本已將php從`v5.5.36`升級至`v7.1.12`(使用lsphp)，手動編譯Phalcon Framework `v3.2.4`(_2019最新版v3.4.2_)以下根據本案技術處理相關檔案個別整理以保持日後維護方便性。

### App選單結構

   1. 在`/src/App.tsx`內配置定義StackNavigator的routes結構
```javascript
  const AppStack = createStackNavigator();
```
   2. 側選單相關程式
      1. `/src/coms/screenMapping.ts`：定義選單名稱轉化觸發導航連結位置，使用switch處理
      2. `/src/coms/DrawerHeader.ts`：顯示側選單上方區塊(回上層/主畫面)
      3. `/src/coms/OuterDrawerItem.tsx`：建立Drawer選單結構
      4. `/src/coms/utils.ts`：拆解routes結構產生選單名稱
      5. `/src/screen/DrawMenus.tsx`：
   3. 頁面區塊選單 `/src/screen/chapters/Submenu.tsx`
   4. 每個內容（章節、表單、設計活動）底部的連結 `/src/coms/layouts.tsx`
```javascript
   // 101行
   class BottomMenu extends Component {}
   export const CustomBottomMenu = withNavigation(BottomMenu)
```

### 網站前端

置頂選單的配置使用靜態文字直接改在`/apps/frontend/views/partials/topnav.volt`內
   > 補充：手機版側邊選單採用抓資料後對應設定輸出文字作法處理。


### 影片轉檔

[網站部分](./page1/webvideo.md)

