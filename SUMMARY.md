# Summary

* [簡介](README.md)
* [開發要點](code/index.md)
  * [程式檔案結構說明](code/page1.md)
    * [ReactNavigation(v2.18)選單配置概念](code/rnnav2.md)
  * [程式片段對應後台編輯欄位設計說明](code/page2.md)
  * [更新轉檔程式紀錄](code/ffmpeg.md)
  * [ffmpeg使用參數說明](code/ffmpeg-param.md)
  * [使用ExpoSDK31搭配Turtle CLI](code/expo31turtle.md)

### React-Native Mobile App

* [PRTElearning操作要點](app/index.md)
  * 如何安裝應用程式
    * [iOS版本](app/install-ios.md)
  * [手機操作說明](app/page1.md)
  * [平板操作說明](app/page2.md)

### PHP7 / Phalcon 3.2.4

* [網站後台操作說明](web/index.md)
  * [系統管理](web/page1.md)
  * [使用帳號](web/page2.md)
  * [計畫內容建立說明](web/page3.md)
    * [各要點內容對應表單](web/page3-1.md)
  * [表單內容建立說明](web/page4.md)
    * [日常生活作息表](web/page4-1.md)
    * [執行表與自我評核表](web/page4-2.md)
    * [家長設計活動](web/page4-3.md)
  * [內容影片](web/page5.md)
    * [上傳影片](web/page5-1.md)
    * [影片檢視與操作](web/page5-2.md)
* [常見問題](FAQ.md)